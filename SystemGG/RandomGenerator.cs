﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

// this part generates the random seed we'll use for keypair generation etc.
// to-do: check this later again and ensure this is a secure way to generate randoms numbers.

namespace SystemGG
{
    class RandomGenerator
    {
        // the random number provider.
        private RNGCryptoServiceProvider Random = new RNGCryptoServiceProvider();

        // return a random integer between a min and max value.
        private int RandomInteger(int min, int max)
        {
            uint scale = uint.MaxValue;
            while (scale == uint.MaxValue)
            {
                // get four random bytes.
                byte[] four_bytes = new byte[4];
                Random.GetBytes(four_bytes);

                // covert that into an uint.
                scale = BitConverter.ToUInt32(four_bytes, 0);
            }

            // add min to the scaled difference between max and min.
            return (int)(min + (max - min) * (scale / (double)uint.MaxValue));
        }
    }
}
